import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class ContactReader {

    public static List<Contact> parseContacts(String fileName) {

        List<Contact> contactList = new ArrayList<Contact>();

        Contact contact = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));

            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();

                    if (startElement.getName().getLocalPart().equals("contact")) {
                        contact = new Contact();
                    }
                    //set name tag text
                    if (startElement.getName().getLocalPart().equals("name")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        contact.setName(xmlEvent.asCharacters().getData());
                    }
                    //set email tag text
                    if (startElement.getName().getLocalPart().equals("email")) {
                        xmlEvent = xmlEventReader.nextEvent();
                        contact.setEmail(xmlEvent.asCharacters().getData());
                    }
                }
                //if Contact end element is reached, add contact object to list
                if (xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("contact")) {
                        contactList.add(contact);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        return contactList;
    }
}