/*Задача №2
Написать консольное приложение используя язык программирования Java для записи данных из XML в базу данных (MySQL).

Требования:
1. Приложение создает новую БД (имя базы данных - фамилия_имя)
2. Приложение создает таблицу в базе данных в которую будет записывать данные из *.xml файла.
3. Необходимо реализовать удобный интерфейс переключения между серверами баз данных.
(Имя сервера, URL драйвера и тд. вынести константами)
4. Файл *.xml должен находиться в корне приложения
 */

import java.util.List;

public class Application {
    static final String DB_URL = "jdbc:mysql://localhost:3306/";
    static final String USER = "root";
    static final String PASS = "";
    static final String DB_NAME = "Smith_John";

    public static void main(String[] args) throws ClassNotFoundException {
        DBExecutor dbe = new DBExecutor(DB_NAME, DB_URL, USER, PASS);
        dbe.createDB();
        dbe.createTable();

        List<Contact> cont = ContactReader.parseContacts("db_data.xml");

        for (Contact c : cont) {
            dbe.insertData(c.getName(), c.getEmail());
        }
    }
}