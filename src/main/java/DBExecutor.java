import java.sql.*;

public class DBExecutor {
    Connection conn = null;
    Statement stmt = null;

    private String DB_URL;
    private String USER;
    private String PASS;
    private String DB_NAME;
    public String dbURL;

    public DBExecutor(String dbName, String db_url, String user, String password) throws ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.DB_NAME = dbName;
        this.DB_URL = db_url;
        this.USER = user;
        this.PASS = password;
        dbURL = DB_URL + DB_NAME;
    }

    public void insertData(String name, String email){
        try{
            conn = DriverManager.getConnection(dbURL, USER, PASS);

            stmt = conn.createStatement();

            String insertData = "INSERT INTO CONTACTS (name, email) \n" +
                                " VALUES ('" + name + "','" + email + "');";

            stmt.executeUpdate(insertData);

            System.out.println("Data inserted successfully...");
        }catch(SQLException se){ //Handle errors for JDBC
            se.printStackTrace();
        }finally { //finally block used to close resources
            try { stmt.close(); } catch (SQLException se2) { /*can't do anything */ }
            try { conn.close(); } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void createTable(){
        try{
            conn = DriverManager.getConnection(dbURL, USER, PASS);

            System.out.println("Creating table in given database...");
            stmt = conn.createStatement();

            String myTable = "CREATE TABLE CONTACTS" +
                            "(id INTEGER AUTO_INCREMENT , " +
                            " name VARCHAR(255), " +
                            " email VARCHAR(255), " +
                            " PRIMARY KEY ( id ))";

            stmt.executeUpdate(myTable);

            System.out.println("Table created successfully...");
        }catch(SQLException se){ //Handle errors for JDBC
            se.printStackTrace();
        }finally { //finally block used to close resources
            try { stmt.close(); } catch (SQLException se2) { /*can't do anything */ }
            try { conn.close(); } catch (SQLException se) { /*can't do anything */ }
        }
    }

    public void createDB(){
        try{
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            System.out.println("Creating database...");
            stmt = conn.createStatement();

            String sql = "CREATE DATABASE " + DB_NAME;
            stmt.executeUpdate(sql);

            System.out.println("Database created successfully...");
        }catch(SQLException se){ //Handle errors for JDBC
            se.printStackTrace();
        }finally { //finally block used to close resources
            try { stmt.close(); } catch (SQLException se2) { /*can't do anything */ }
            try { conn.close(); } catch (SQLException se) { /*can't do anything */ }
        }
    }
}